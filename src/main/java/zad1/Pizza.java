package zad1;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Pizza {


    public enum Size {
        SMALL, MEDIUM, LARGE;
    }


    public enum Extra {
        HAM, CHAMPIGNON_MUSHROOM, ONION, PEPPER, SAUSAGE, CHEESE;
    }


    public enum Base {
        TOMATO_SAUCE, CREAM_SAUCE;
    }


    public enum SaucePosition {
        INSIDE, OUTSIDE;
    }

    private Size size;
    private List<Extra> extras;
    private Base base;
    private SaucePosition saucePosition;


    Pizza(Size size, List<Extra> extras, Base base) {
        this.size = size;
        this.extras = extras;
        this.base = base;
    }

    public void setSaucePosition(SaucePosition saucePosition) {
        this.saucePosition = saucePosition;
    }

}
