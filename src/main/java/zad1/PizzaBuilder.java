package zad1;

import java.util.ArrayList;
import java.util.List;

public class PizzaBuilder {

    private Pizza.Size size;
    private List<Pizza.Extra> extras;
    private Pizza.Base base;
    private Pizza.SaucePosition saucePosition;

    public PizzaBuilder() {
        extras = new ArrayList<Pizza.Extra>();
    }

    public PizzaBuilder small() {
        size = Pizza.Size.SMALL;
        return this;
    }

    public PizzaBuilder medium() {
        size = Pizza.Size.MEDIUM;
        return this;
    }

    public PizzaBuilder large() {
        size = Pizza.Size.LARGE;
        return this;
    }

    public PizzaBuilder addExtra(Pizza.Extra extra) {
        extras.add(extra);
        return this;
    }

    public PizzaBuilder tomatoBase() {
        base = Pizza.Base.TOMATO_SAUCE;
        return this;
    }

    public PizzaBuilder creamBase() {
        base = Pizza.Base.CREAM_SAUCE;
        return this;
    }

    public PizzaBuilder sauceOutside() {
        saucePosition = Pizza.SaucePosition.OUTSIDE;
        return this;
    }

    public PizzaBuilder sauceInside() {
        saucePosition = Pizza.SaucePosition.INSIDE;
        return this;
    }

    public Pizza build() {
        Pizza pizza = new Pizza(size, extras, base);
        pizza.setSaucePosition(saucePosition);
        return pizza;
    }

}
