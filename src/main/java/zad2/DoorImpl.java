package zad2;

import zad2.interfaces.Door;

public class DoorImpl implements Door {
    public void open() {
        System.out.println("Door open");
    }

    public void close() {
        System.out.println("Door close");
    }

    public void lock() {
        System.out.println("Door lock");
    }

    public void unlock() {
        System.out.println("Door unlock");
    }
}
