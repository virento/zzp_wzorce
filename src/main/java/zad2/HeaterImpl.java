package zad2;

import zad2.interfaces.Heater;

public class HeaterImpl implements Heater {

    public void on() {
        System.out.println("Heater ON");
    }

    public void off() {
        System.out.println("Heater OFF");
    }

    public void setTemperature(Double tempC) {
        System.out.println("Heater temperature set to: " + tempC.toString());
    }
}
