package zad2;

import zad2.interfaces.Door;
import zad2.interfaces.Heater;
import zad2.interfaces.Light;

public class HouseAPI {

    private Door door = new DoorImpl();
    private Light light = new LightImpl();
    private Heater heater = new HeaterImpl();

    public HouseAPI() {

    }

    public void lockHouse() {
        door.close();
        door.lock();
        light.off();
        heater.off();
    }

    public void unlockHouse() {
        door.unlock();
        light.on();
        heater.setTemperature(22d);
        heater.on();
    }

    public void openHouseDoor() {
        door.unlock();
        door.open();
        light.on();
    }

    public void closeHouseDoor() {
        door.close();
        light.off();
    }

}
