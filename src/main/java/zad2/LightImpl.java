package zad2;

import zad2.interfaces.Light;

public class LightImpl implements Light {
    public void on() {
        System.out.println("Light ON");
    }

    public void off() {
        System.out.println("Light OFF");
    }
}
