package zad2.decorator;

public interface DecoratorInterface {

    void action();

    String getDescription();

}
