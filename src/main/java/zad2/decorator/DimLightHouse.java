package zad2.decorator;

public class DimLightHouse implements DecoratorInterface {

    private DecoratorInterface decoratorInterface;

    public DimLightHouse(DecoratorInterface decorator) {
        this.decoratorInterface = decorator;
    }

    public void action() {
        System.out.println("DimLightHouse action");
        decoratorInterface.action();
    }

    public String getDescription() {
        return "Dim Light house, " + decoratorInterface.getDescription();
    }
}
