package zad2.decorator;

public class EntryGateCloseDecorator implements DecoratorInterface {

    private DecoratorInterface decoratorInterface;

    public EntryGateCloseDecorator(DecoratorInterface decorator) {
        this.decoratorInterface = decorator;
    }

    public void action() {
        System.out.println("EntryGateCloseDecorator action");
        decoratorInterface.action();
    }

    public String getDescription() {
        return "Entry gate close, " + decoratorInterface.getDescription();
    }
}
