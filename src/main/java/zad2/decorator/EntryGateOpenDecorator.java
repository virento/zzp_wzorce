package zad2.decorator;

public class EntryGateOpenDecorator implements DecoratorInterface {

    private DecoratorInterface decoratorInterface;

    public EntryGateOpenDecorator(DecoratorInterface decorator) {
        this.decoratorInterface = decorator;
    }

    public void action() {
        System.out.println("EntryGateOpenDecorator action");
        decoratorInterface.action();
    }

    public String getDescription() {
        return "Entry gate open, " + decoratorInterface.getDescription();
    }
}
