package zad2.decorator;

public class HomeCinemaDynamicLight implements DecoratorInterface {

    private DecoratorInterface decoratorInterface;

    public HomeCinemaDynamicLight(DecoratorInterface decorator) {
        this.decoratorInterface = decorator;
    }

    public void action() {
        System.out.println("HouseCinemaDynamicLight action");
        decoratorInterface.action();
    }

    public String getDescription() {
        return "House cinema dynamic light, " + decoratorInterface.getDescription();
    }
}
