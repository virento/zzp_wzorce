package zad2.decorator;

public class HomeCinemaLight implements DecoratorInterface {

    private DecoratorInterface decoratorInterface;

    public HomeCinemaLight(DecoratorInterface decorator) {
        this.decoratorInterface = decorator;
    }

    public void action() {
        System.out.println("HomeCinemaLight");
        decoratorInterface.action();
    }

    public String getDescription() {
        return "Home cinema light, " + decoratorInterface.getDescription();
    }
}
