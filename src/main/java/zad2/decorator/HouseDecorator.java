package zad2.decorator;

public class HouseDecorator implements DecoratorInterface {

    public HouseDecorator() {

    }

    public void action() {
        System.out.println("House action");
    }

    public String getDescription() {
        return "House";
    }
}
