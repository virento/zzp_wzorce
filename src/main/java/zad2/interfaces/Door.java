package zad2.interfaces;

public interface Door {
    void open();

    void close();

    void lock();

    void unlock();
}
