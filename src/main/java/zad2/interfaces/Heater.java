package zad2.interfaces;

public interface Heater {

    void on();

    void off();

    void setTemperature(Double tempC);
}
