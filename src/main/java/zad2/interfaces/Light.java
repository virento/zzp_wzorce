package zad2.interfaces;

public interface Light {

    void on();

    void off();
}
