package zad3;

import zad3.interfaces.Command;
import zad3.interfaces.Observer;
import zad3.interfaces.Subject;

import java.util.ArrayList;
import java.util.List;

public class Kiosk implements Subject {

    private ArrayList<Command> orders = new ArrayList<Command>();
    private List<Observer> observers = new ArrayList<Observer>();

    public Kiosk() {

    }

    public void addOrder(Command order) {
        orders.add(order);
        notifyAllObservers();
    }

    public void executeOrder(Command order) {
        if (orders.size() == 0) {
            return;
        }
        Command command = orders.get(0);
        command.run();
        orders.remove(command);
    }

    public void register(Observer observer) {
        observers.add(observer);
    }

    public void notifyAllObservers() {
        observers.forEach(Observer::update);
    }
}
