package zad3;

import zad3.interfaces.Observer;

public class Kitchen implements Observer {

    public Kitchen() {

    }

    public void update() {
        System.out.println("Next Order");
    }
}
