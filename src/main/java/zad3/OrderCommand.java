package zad3;

import zad3.interfaces.Command;
import zad3.interfaces.PromotionStrategy;

import java.util.ArrayList;
import java.util.List;

public class OrderCommand implements Command {

    private PromotionStrategy promotion;
    private Double totalPrice = 0d;
    List<Product> products = new ArrayList<Product>();

    public OrderCommand() {

    }

    public void addProduct(Product product) {
        products.add(product);
        totalPrice += product.getPrice();
        if (promotion != null) {
            return;
        }
        if (product.getQuantity() > 2) {
            promotion = new Promotion10();
        }
        if (totalPrice > 30.00d) {
            promotion = new Promotion30();
        }
    }

    public void run() {
        if (promotion != null) {
            promotion.applyPromotion(products);
        }
        System.out.print("Order: ");
        for (Product product : products) {
            System.out.printf("%dx %s (%f zł)", product.getQuantity(), product.getName(), product.getPrice());
        }
        System.out.println();
    }
}
