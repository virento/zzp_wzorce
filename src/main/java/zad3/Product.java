package zad3;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {

    private String name;
    private Double priceForOne;
    private Integer quantity;
    private Double price;

    public Product(String name, Double priceForOne, Integer quantity) {
        this.name = name;
        this.priceForOne = priceForOne;
        this.quantity = quantity;
        this.price = priceForOne * quantity;
    }

    public Double getPrice() {
        return price;
    }

}
