package zad3;

import zad3.interfaces.PromotionStrategy;

import java.util.List;

public class Promotion10 implements PromotionStrategy {

    public void applyPromotion(List<Product> products) {
        Product matchingProduct = products.get(0);
        for (int i = 1; i < products.size(); i++) {
            Product product = products.get(i);
            if (product.getQuantity() > 0 && product.getPrice() < matchingProduct.getPrice()) {
                matchingProduct = product;
            }
        }
        matchingProduct.setPrice(matchingProduct.getPrice() * 0.9d);
    }
}
