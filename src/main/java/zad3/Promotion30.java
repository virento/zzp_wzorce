package zad3;

import zad3.interfaces.PromotionStrategy;

import java.util.List;

public class Promotion30 implements PromotionStrategy {

    public void applyPromotion(List<Product> products) {
        for (Product product : products) {
            if (product.getName().equals("NapujGazowany")) {
                product.setPrice(0.01d);
                return;
            }
        }
    }
}
