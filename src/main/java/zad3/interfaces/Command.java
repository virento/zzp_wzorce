package zad3.interfaces;

public interface Command {

    void run();
}
