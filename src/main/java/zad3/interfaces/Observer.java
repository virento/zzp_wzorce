package zad3.interfaces;

public interface Observer {

    void update();

}
