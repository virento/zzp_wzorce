package zad3.interfaces;

import zad3.Product;

import java.util.List;

public interface PromotionStrategy {

    void applyPromotion(List<Product> products);

}
