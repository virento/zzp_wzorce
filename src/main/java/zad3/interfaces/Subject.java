package zad3.interfaces;

public interface Subject {

    void register(Observer observer);

    void notifyAllObservers();

}
